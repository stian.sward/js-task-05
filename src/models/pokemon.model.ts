export interface PokemonListResponse {
    count: number,
    next: string,
    previous: string,
    results: [{
        name: string,
        url: string
    }]
}

export interface PokemonCatalogueItem {
    name: string,
    url: string,
    id?: number,
    image?: string
}

export interface PokemonCardItem {
    abilities?: string[],
    base_experience?: number,
    base_stats?: [{
        name?:string,
        value?: number
    }]
    height?: number,
    id: number,
    image?: string,
    moves?: string[],
    name?: string,
    types?: string[],
    weight?: number
}

export interface Move {
    id?: number,
    url: string,
    name: string,
    accuracy?: number,
    damage_class?: string,
    power?: number,
    pp?: number,
    type?: string
}

export interface PokemonJsonResponse {
    abilities: [{
        ability: {
            name: string,
            url: string
        }
    }],
    base_experience: number,
    height: number,
    id: number,
    moves: [{
        move: {
            name: string,
            url: string
        }
    }],
    name: string,
    stats: [{
        base_stat: number,
        stat: {
            name: string
        }
    }],
    types: [{
        type: {
            name: string,
            url: string
        }
    }],
    weight: number
}