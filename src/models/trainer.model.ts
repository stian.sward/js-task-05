import { PokemonCardItem } from './pokemon.model';

export interface Trainer {
    name: string,
    pokemon: PokemonCardItem[]
}