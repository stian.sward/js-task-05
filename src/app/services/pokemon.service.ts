import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { PokemonCardItem, PokemonCatalogueItem, PokemonJsonResponse, PokemonListResponse } from 'src/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  public nextUrl: string = '';
  public prevUrl: string = '';

  constructor(private http: HttpClient) { }

  getAllPokemon(url: string = 'https://pokeapi.co/api/v2/pokemon?limit=40'): Observable<PokemonCatalogueItem[]> {
    return this.http.get<PokemonListResponse>(url)
      .pipe(
        map((response: PokemonListResponse) => {
          return this.mapPokemonCatalogue(response);
        })
      );
  }

  mapPokemonCatalogue(list: PokemonListResponse): PokemonCatalogueItem[] {
    const catalogue: PokemonCatalogueItem[] = [];
    let id: number = 0;
    list.results.forEach(pokemon => {
      id = parseInt(pokemon.url.split( '/' ).filter( Boolean ).pop());
      catalogue.push({name: pokemon.name, url: pokemon.url, id: id,
        image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`});
    });
    this.nextUrl = list.next;
    this.prevUrl = list.previous;
    return catalogue;
  }

  getPokemonDetails(id: string): PokemonCardItem {
    let pokemon: PokemonCardItem = {id: parseInt(id)};
    this.http.get<PokemonJsonResponse>(`https://pokeapi.co/api/v2/pokemon/${id}`)
      .pipe(map(res => res))
      .subscribe(res => {
        pokemon.base_experience = res.base_experience;
        pokemon.height = res.height;
        pokemon.weight = res.weight;
        pokemon.name = this.toTitleCase(res.name);
        pokemon.image = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
        // Add base stats
        pokemon.base_stats = [{}]; // Cannot instantiate with empty array so need a dummy element
        res.stats.forEach(stat => {
          pokemon.base_stats.push({name: stat.stat.name, value: stat.base_stat});
        });
        pokemon.base_stats.shift(); // And then delete it once the array is filled
        // Add abilities
        pokemon.abilities = [];
        res.abilities.forEach(ability => {
          pokemon.abilities.push(ability.ability.name);
        });
        // Add moves
        pokemon.moves = [];
        res.moves.forEach(move => {
          pokemon.moves.push(move.move.name);
        });
        // Add types
        pokemon.types = [];
        res.types.forEach(type => {
          pokemon.types.push(type.type.name);
        })
      });
    return pokemon;
  }

  toTitleCase(lower: string): string {
    let sentence: string[] = lower.toLowerCase().split(' ');
    for(let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    return sentence.join(' ');
  }
}
