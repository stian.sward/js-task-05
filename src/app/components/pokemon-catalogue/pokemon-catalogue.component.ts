import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokemonCatalogueItem } from 'src/models/pokemon.model';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.scss']
})
export class PokemonCatalogueComponent implements OnInit {

  public catalogue: PokemonCatalogueItem[] = [];
  public catalogue$: Subscription;
  public error: string = '';

  constructor(
    public pokemonService: PokemonService,
    private router: Router) {}

  ngOnInit(): void {
    this.fetchPokemon();
  }

  fetchPokemon(url?: string): void {
    this.pokemonService.getAllPokemon(url).subscribe((catalogue: PokemonCatalogueItem[]) => {
      this.catalogue = this.catalogue.concat(catalogue);
    }, error => {
      this.error = error.message;
    });
  }

  openPokemonDetails(id: number): void {
    this.router.navigateByUrl(`/pokemon/${id}`);
  }

  onScroll() {
    this.fetchPokemon(this.pokemonService.nextUrl);
  }
}
 