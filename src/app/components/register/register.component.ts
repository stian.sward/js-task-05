import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public ngStyle = {boxShadow: '0 0 0.2em #2424a8'};

  constructor(private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('trainerName')) {
      this.router.navigateByUrl('/pokemon');
    }
  }

  onRegisterClick(name: string): void {
    if (name == '') {
      console.log('Empty!');
      return;
    }
    localStorage.setItem('trainerName', name);
    localStorage.setItem('pokemon', "[]");
    window.location.reload();
    this.router.navigateByUrl('/pokemon');
  }
}
