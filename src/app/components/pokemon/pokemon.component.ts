import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokemonCardItem } from 'src/models/pokemon.model';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {

  public pokemon: PokemonCardItem;

  public statAbbreviations = {
    'hp': 'HP',
    'attack': 'ATK',
    'defense': 'DEF',
    'special-attack': 'SP.ATK',
    'special-defense': 'SP.DEF',
    'speed': 'SPEED'
  }

  public typeColors = {
    bug: '#A8B820',
    dark: '#705848',
    dragon: '#7038F8',
    electric: '#F8D030',
    fairy: '#EE99AC',
    fighting: '#C03028',
    fire: '#F08030',
    flying: '#A890F0',
    ghost: '#705898',
    grass: '#A7DB8D',
    ground: '#E0C068',
    ice: '#98D8D8',
    normal: '#C6C6A7',
    poison: '#A040A0',
    psychic: '#F85888',
    rock: '#B8A038',
    shadow: '#2C2E2B',
    steel: '#B8B8D0',
    unknown: '#68A090',
    water: '#6890F0'
}

  constructor(
    public pokemonService: PokemonService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.pokemon = this.pokemonService.getPokemonDetails(this.route.snapshot.paramMap.get('id'));
  }

  catchPokemon(): void {
    let pokemonList = JSON.parse(localStorage.getItem('pokemon'));
    pokemonList.push(this.pokemon);
    localStorage.setItem('pokemon', JSON.stringify(pokemonList));
    this.router.navigateByUrl('/pokemon');
  }
}
