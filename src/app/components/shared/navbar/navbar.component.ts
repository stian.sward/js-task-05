import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public trainerName: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.trainerName = localStorage.getItem('trainerName');
  }

  onClick(dest: string) {
    this.router.navigateByUrl(dest);
  }

  logout(): void {
    localStorage.removeItem('trainerName');
    localStorage.removeItem('pokemon');
    this.trainerName = undefined;
    this.router.navigateByUrl('register');
  }

  trainerPage(): void {
    this.router.navigateByUrl('trainer');
  }
}
