import { Component, OnInit } from '@angular/core';
import { PokemonCardItem } from 'src/models/pokemon.model';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {

  trainerName: string = '';
  public pokemonCollection: PokemonCardItem[];

  constructor() { }

  ngOnInit(): void {
    this.trainerName = localStorage.getItem('trainerName');
    this.pokemonCollection = JSON.parse(localStorage.getItem('pokemon'));
  }

  onRelease(id: number): void {
    console.log('releasing pokemon', id);
    
    this.pokemonCollection.splice(id, 1);
    localStorage.setItem('pokemon', JSON.stringify(this.pokemonCollection));
  }
}
